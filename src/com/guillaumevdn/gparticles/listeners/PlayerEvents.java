package com.guillaumevdn.gparticles.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.item.ItemUtils;
import com.guillaumevdn.gcore.lib.number.NumberUtils;
import com.guillaumevdn.gcore.lib.string.placeholder.Replacer;
import com.guillaumevdn.gparticles.ConfigGP;
import com.guillaumevdn.gparticles.lib.gadget.active.GadgetUtils;
import com.guillaumevdn.gparticles.lib.gadget.element.ElementGadget;
import com.guillaumevdn.gparticles.lib.gadget.element.GadgetType;
import com.guillaumevdn.gparticles.lib.gui.ElementGUIGP;

/**
 * @author GuillaumeVDN
 */
public class PlayerEvents implements Listener {

	@EventHandler
	public void event(PlayerJoinEvent event) {
		setInventory(event.getPlayer());
	}

	private void setInventory(Player player) {
		if (NumberUtils.isInRange(ConfigGP.hotbarSlotMenu, 0, 8)) {
			final ItemStack existing = player.getInventory().getItem(ConfigGP.hotbarSlotMenu);
			player.getInventory().setItem(ConfigGP.hotbarSlotMenu, ConfigGP.hotbarItemMenu);

			if (Mat.fromItem(existing).orAir().isAir() || existing.isSimilar(ConfigGP.hotbarItemMenu)) {
				player.updateInventory();
			} else {
				ItemUtils.give(player, existing, true);
			}
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)  // lobby plugins are likely to cancel any drop without checks, which is more performant
	public void event(PlayerDropItemEvent event) {
		final ItemStack item = event.getItemDrop().getItemStack();
		if (ConfigGP.hotbarItemMenu.isSimilar(item) || GadgetUtils.isGadgetItem(item)) {
			event.setCancelled(true);
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
	public void event(InventoryClickEvent event) {
		final Player player = (Player) event.getWhoClicked();
		final ItemStack item = event.getCurrentItem();

		if (item != null && clickItem(player, item)) {
			event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void event(PlayerInteractEvent event) {
		final Player player = event.getPlayer();
		final ItemStack item = event.getItem();

		if (item != null && clickItem(player, item)) {
			event.setCancelled(true);
		}
	}

	private boolean clickItem(Player player, ItemStack item) {
		if (ConfigGP.hotbarItemMenu.isSimilar(item)) {
			final ElementGUIGP gui = ConfigGP.guis.getElement(ConfigGP.mainGUIId).orNull();
			if (gui != null) {
				gui.build(Replacer.of(player)).openFor(player, null);
			}

			return true;
		}

		final GadgetType gadgetType = GadgetUtils.getGadgetType(item);
		if (gadgetType != null) {
			final ElementGadget gadget = ConfigGP.gadgets.get(gadgetType);
			if (gadget != null) {
				gadgetType.attemptStart(gadget, player);
			}

			return true;
		}

		return false;
	}

}
