package com.guillaumevdn.gparticles;

import com.guillaumevdn.gcore.lib.string.TextElement;
import com.guillaumevdn.gcore.lib.string.TextEnumElement;

/**
 * @author GuillaumeVDN
 */
public enum TextGP implements TextEnumElement {

	// commands
	commandDescriptionGparticles,
	commandDescriptionGparticlesEdit,
	commandDescriptionGparticlesParticle,
	commandDescriptionGparticlesTrail,
	commandDescriptionGparticlesGadget,

	commandParameterUsageParticle,
	commandParameterUsageTrail,
	commandParameterUsageGadget,

	// particles
	messageParticleEnable,
	messageParticleDisable,

	// trails
	messageTrailEnable,
	messageTrailDisable,

	// gadgets
	messageGadgetSetHotbar,
	messageGadgetUsed,
	messageGadgetCooldown,
	;

	private TextElement text = new TextElement();

	TextGP() {
	}

	@Override
	public String getId() {
		return name();
	}

	@Override
	public TextElement getText() {
		return text;
	}

}
