package com.guillaumevdn.gparticles.command;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcore.TextGeneric;
import com.guillaumevdn.gcore.lib.collection.CollectionUtils;
import com.guillaumevdn.gcore.lib.command.CommandCall;
import com.guillaumevdn.gcore.lib.command.Subcommand;
import com.guillaumevdn.gcore.lib.command.argument.ArgumentPlayer;
import com.guillaumevdn.gcore.lib.object.NeedType;
import com.guillaumevdn.gcore.lib.string.placeholder.Replacer;
import com.guillaumevdn.gparticles.PermissionGP;
import com.guillaumevdn.gparticles.TextGP;
import com.guillaumevdn.gparticles.data.user.UserGP;
import com.guillaumevdn.gparticles.lib.command.ArgumentTrail;
import com.guillaumevdn.gparticles.lib.gui.item.type.TypeTrail;
import com.guillaumevdn.gparticles.lib.trail.element.ElementTrail;

/**
 * @author GuillaumeVDN
 */
public class CmdTrail extends Subcommand {

	private final ArgumentPlayer argPlayer = addArgumentPlayer(NeedType.OPTIONAL, false, PermissionGP.inst().gparticlesCommandTrailOthers, TextGeneric.commandParameterUsageTarget, true);
	private final ArgumentTrail argTrail = addArgument(new ArgumentTrail(NeedType.OPTIONAL, false, null, TextGP.commandParameterUsageTrail));

	public CmdTrail() {
		super(false, PermissionGP.inst().gparticlesCommandTrail, TextGP.commandDescriptionGparticlesTrail, CollectionUtils.asList("trail"));
	}

	@Override
	public void perform(CommandCall call) {
		final Player player = argPlayer.get(call);
		final UserGP user = UserGP.cachedOrNull(player);
		if (user == null)
			return;

		final ElementTrail trail = argTrail.get(call);
		if (trail == null) {
			user.setActiveTrail(null);
			TextGP.messageTrailDisable.send(player);
		} else
			TypeTrail.clickOnTrail(player, user, trail, Replacer.justPlayer(player));
	}

}
