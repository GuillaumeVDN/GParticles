package com.guillaumevdn.gparticles.command;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcore.TextGeneric;
import com.guillaumevdn.gcore.lib.collection.CollectionUtils;
import com.guillaumevdn.gcore.lib.command.CommandCall;
import com.guillaumevdn.gcore.lib.command.Subcommand;
import com.guillaumevdn.gcore.lib.command.argument.ArgumentPlayer;
import com.guillaumevdn.gcore.lib.object.NeedType;
import com.guillaumevdn.gcore.lib.string.placeholder.Replacer;
import com.guillaumevdn.gparticles.PermissionGP;
import com.guillaumevdn.gparticles.TextGP;
import com.guillaumevdn.gparticles.data.user.UserGP;
import com.guillaumevdn.gparticles.lib.command.ArgumentParticle;
import com.guillaumevdn.gparticles.lib.gui.item.type.TypeParticle;
import com.guillaumevdn.gparticles.lib.particle.ElementParticleGP;

/**
 * @author GuillaumeVDN
 */
public class CmdParticle extends Subcommand {

	private final ArgumentPlayer argPlayer = addArgumentPlayer(NeedType.OPTIONAL, false, PermissionGP.inst().gparticlesCommandParticleOthers, TextGeneric.commandParameterUsageTarget, true);
	private final ArgumentParticle argParticle = addArgument(new ArgumentParticle(NeedType.OPTIONAL, false, null, TextGP.commandParameterUsageParticle));

	public CmdParticle() {
		super(false, PermissionGP.inst().gparticlesCommandParticle, TextGP.commandDescriptionGparticlesParticle, CollectionUtils.asList("particle"));
	}

	@Override
	public void perform(CommandCall call) {
		final Player player = argPlayer.get(call);
		final UserGP user = UserGP.cachedOrNull(player);

		if (user == null)
			return;

		final ElementParticleGP particle = argParticle.get(call);
		if (particle == null) {
			user.setActiveParticle(null);
			TextGP.messageParticleDisable.send(player);
		} else
			TypeParticle.clickOnParticle(player, user, particle, Replacer.justPlayer(player));
	}

}
