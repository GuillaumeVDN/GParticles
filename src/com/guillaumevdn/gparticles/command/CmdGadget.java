package com.guillaumevdn.gparticles.command;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcore.TextGeneric;
import com.guillaumevdn.gcore.lib.collection.CollectionUtils;
import com.guillaumevdn.gcore.lib.command.CommandCall;
import com.guillaumevdn.gcore.lib.command.Subcommand;
import com.guillaumevdn.gcore.lib.command.argument.ArgumentPlayer;
import com.guillaumevdn.gcore.lib.object.NeedType;
import com.guillaumevdn.gcore.lib.permission.Permission;
import com.guillaumevdn.gcore.lib.string.placeholder.Replacer;
import com.guillaumevdn.gparticles.ConfigGP;
import com.guillaumevdn.gparticles.PermissionGP;
import com.guillaumevdn.gparticles.TextGP;
import com.guillaumevdn.gparticles.data.user.UserGP;
import com.guillaumevdn.gparticles.lib.command.ArgumentGadgetType;
import com.guillaumevdn.gparticles.lib.gadget.element.ElementGadget;
import com.guillaumevdn.gparticles.lib.gadget.element.GadgetType;

/**
 * @author GuillaumeVDN
 */
public class CmdGadget extends Subcommand {

	private final ArgumentPlayer argPlayer = addArgumentPlayer(NeedType.OPTIONAL, false, PermissionGP.inst().gparticlesCommandGadgetOthers, TextGeneric.commandParameterUsageTarget, true);
	private final ArgumentGadgetType argGadget = addArgument(new ArgumentGadgetType(NeedType.REQUIRED, false, null, TextGP.commandParameterUsageGadget));

	public CmdGadget() {
		super(false, PermissionGP.inst().gparticlesCommandGadget, TextGP.commandDescriptionGparticlesGadget, CollectionUtils.asList("gadget"));
	}

	@Override
	public void perform(CommandCall call) {
		final Player player = argPlayer.get(call);
		final UserGP user = UserGP.cachedOrNull(player);
		if (user == null)
			return;

		final GadgetType gadgetType = argGadget.get(call);
		final ElementGadget gadget = gadgetType == null ? null : ConfigGP.gadgets.get(gadgetType);
		if (gadget == null) {
		} else {
			final Permission permission = gadget.getPermission().directParseOrNull(Replacer.justPlayer(player));
			if (permission == null || permission.has(player))
				gadget.getType().attemptStart(gadget, player);
			else
				TextGeneric.messageNoPermission.send(call);
		}
	}

}
