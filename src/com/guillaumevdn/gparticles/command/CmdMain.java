package com.guillaumevdn.gparticles.command;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcore.lib.collection.CollectionUtils;
import com.guillaumevdn.gcore.lib.command.CommandCall;
import com.guillaumevdn.gcore.lib.command.Subcommand;
import com.guillaumevdn.gcore.lib.string.placeholder.Replacer;
import com.guillaumevdn.gparticles.ConfigGP;
import com.guillaumevdn.gparticles.PermissionGP;
import com.guillaumevdn.gparticles.TextGP;
import com.guillaumevdn.gparticles.lib.gui.ElementGUIGP;

/**
 * @author GuillaumeVDN
 */
public class CmdMain extends Subcommand {

	public CmdMain() {
		super(true, PermissionGP.inst().gparticlesCommandMain, TextGP.commandDescriptionGparticles, CollectionUtils.asList("gparticles"));
	}

	@Override
	public void perform(CommandCall call) {
		final Player player = call.getSenderPlayer();
		final ElementGUIGP gui = ConfigGP.guis.getElement(ConfigGP.mainGUIId).orNull();

		if (gui != null) {
			gui.build(Replacer.of(player)).openFor(player, null);
		}
	}

}
