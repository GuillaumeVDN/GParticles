package com.guillaumevdn.gparticles;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.inventory.ItemStack;

import com.guillaumevdn.gcore.lib.GPluginConfig;
import com.guillaumevdn.gcore.lib.collection.CollectionUtils;
import com.guillaumevdn.gcore.lib.configuration.YMLConfiguration;
import com.guillaumevdn.gcore.lib.element.type.container.ElementWorldRestriction;
import com.guillaumevdn.gcore.lib.exception.ConfigError;
import com.guillaumevdn.gparticles.lib.gadget.element.ElementGadget;
import com.guillaumevdn.gparticles.lib.gadget.element.GadgetType;
import com.guillaumevdn.gparticles.lib.gadget.element.GadgetTypes;
import com.guillaumevdn.gparticles.lib.gui.GUIsContainer;
import com.guillaumevdn.gparticles.lib.particle.ParticlesContainer;
import com.guillaumevdn.gparticles.lib.trail.element.TrailsContainer;

/**
 * @author GuillaumeVDN
 */
public class ConfigGP extends GPluginConfig {

	public static List<String> commandsAliasesEdit;

	public static int hotbarSlotMenu;
	public static int hotbarSlotGadget;
	public static int hotbarSlotColorGun;
	public static ItemStack hotbarItemMenu;

	public static ElementWorldRestriction worldRestriction;
	public static String mainGUIId;
	public static List<String> trailBlocksBlacklist;

	public static ParticlesContainer particles;
	public static TrailsContainer trails;
	public static GUIsContainer guis;
	public static Map<GadgetType, ElementGadget> gadgets;

	@Override
	protected YMLConfiguration doLoad() throws Throwable {
		final YMLConfiguration baseConfig = GParticles.inst().loadConfigurationFile("config.yml");
		final YMLConfiguration gadgetsConfig = GParticles.inst().loadConfigurationFile("gadgets.yml");

		commandsAliasesEdit = CollectionUtils.asLowercaseList(baseConfig.readMandatoryStringList("commands_aliases.edit"));

		hotbarSlotMenu = baseConfig.readMandatoryInteger("hotbar_slot_menu");
		hotbarSlotGadget = baseConfig.readMandatoryInteger("hotbar_slot_gadget");
		hotbarSlotColorGun = baseConfig.readMandatoryInteger("hotbar_slot_color_gun");
		hotbarItemMenu = baseConfig.readMandatoryItemStack("hotbar_item_menu");

		worldRestriction = baseConfig.readElement("world_restriction", ElementWorldRestriction.class);
		mainGUIId = baseConfig.readMandatoryString("main_gui_id");
		trailBlocksBlacklist = baseConfig.readStringList("trail_blocks_blacklist", CollectionUtils.asList("_STAIRS", "SIGN", "CHEST", "_SLAB", "BARRIER", "WALL", "FENCE", "HONEY", "WATER", "LAVA", "WOOD", "LADDER"));

		(particles = new ParticlesContainer()).load();
		(trails = new TrailsContainer()).load();
		(guis = new GUIsContainer()).load();

		gadgets = new HashMap<>();
		for (String key : gadgetsConfig.readKeysForSection("gadgets")) {
			final ElementGadget gadget = gadgetsConfig.readElement("gadgets." + key, ElementGadget.class);
			if (gadgets.containsKey(gadget.getType())) {
				throw new ConfigError("Found multiple gadgets configuration with type '" + gadget.getType().getId() + "'");
			}
			gadgets.put(gadget.getType(), gadget);
		}
		for (GadgetType type : GadgetTypes.inst().values()) {
			if (!gadgets.containsKey(type)) {
				throw new ConfigError("Missing gadget configuration of type '" + type.getId() + "'");
			}
		}


		return baseConfig;
	}

}
