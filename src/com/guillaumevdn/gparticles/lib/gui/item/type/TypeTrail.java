package com.guillaumevdn.gparticles.lib.gui.item.type;

import java.util.Set;
import java.util.function.Consumer;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.guillaumevdn.gcore.TextGeneric;
import com.guillaumevdn.gcore.lib.compatibility.material.CommonMats;
import com.guillaumevdn.gcore.lib.element.struct.Need;
import com.guillaumevdn.gcore.lib.element.struct.parsing.ParsingError;
import com.guillaumevdn.gcore.lib.element.type.container.ElementItemMode;
import com.guillaumevdn.gcore.lib.function.QuadriConsumer;
import com.guillaumevdn.gcore.lib.gui.element.item.ElementGUIItem;
import com.guillaumevdn.gcore.lib.gui.element.item.type.GUIItemType;
import com.guillaumevdn.gcore.lib.gui.element.item.type.IconNeed;
import com.guillaumevdn.gcore.lib.gui.struct.ClickCall;
import com.guillaumevdn.gcore.lib.gui.struct.active.ActiveGUI;
import com.guillaumevdn.gcore.lib.gui.struct.active.ActiveItemHolder;
import com.guillaumevdn.gcore.lib.gui.struct.active.ActiveItemHolderElementGUIItem;
import com.guillaumevdn.gcore.lib.gui.struct.active.ActiveItemHolderElementGUIItemCommon;
import com.guillaumevdn.gcore.lib.gui.struct.active.ItemHolder;
import com.guillaumevdn.gcore.lib.item.ItemUtils;
import com.guillaumevdn.gcore.lib.permission.Permission;
import com.guillaumevdn.gcore.lib.string.StringUtils;
import com.guillaumevdn.gcore.lib.string.placeholder.Replacer;
import com.guillaumevdn.gparticles.TextEditorGP;
import com.guillaumevdn.gparticles.TextGP;
import com.guillaumevdn.gparticles.data.user.UserGP;
import com.guillaumevdn.gparticles.lib.trail.element.ElementTrail;
import com.guillaumevdn.gparticles.lib.trail.element.ElementTrailReference;

/**
 * @author GuillaumeVDN
 */
public class TypeTrail extends GUIItemType {

	public TypeTrail(String id) {
		super(id, IconNeed.REQUIRED, CommonMats.EMERALD);
	}

	@Override
	protected void doFillTypeSpecificElements(ElementGUIItem item) {
		super.doFillTypeSpecificElements(item);
		item.add(new ElementTrailReference(item, "trail", Need.required(), TextEditorGP.elementGUIItemTypeTrailTrail));
		item.addItem("icon_no_permission", Need.optional(), ElementItemMode.BUILDABLE, TextEditorGP.elementGUIItemTypeTrailIconNoPermission);
	}

	@Override
	public ActiveItemHolder newActive(ActiveGUI instance, ItemHolder holder, ElementGUIItem element) {
		return new ActiveItemHolderElementGUIItemCommon(instance, holder, element) {
			@Override
			protected void build(ItemStack itemIcon, QuadriConsumer<ItemStack, Set<String>, Integer, Consumer<ClickCall>> callback) throws ParsingError {

				final Player player = instance.getReplacer().getReplacerData().getPlayer();
				final UserGP user = UserGP.cachedOrNull(player);
				if (user == null)
					return;

				final ElementTrail trail = element.directParseNoCatchOrThrowParsingNull("trail", instance.getReplacer());
				final Permission permission = trail.getPermission().directParseOrNull(instance.getReplacer());

				if (permission != null && !permission.has(player)) {
					itemIcon = element.directParseOrNull("icon_no_permission", instance.getReplacer());
					if (itemIcon == null)
						return;
					itemIcon = ActiveItemHolderElementGUIItem.resetNameLorePlaceholders(itemIcon, element.getElementAs("icon_no_permission"));
				} else if (trail.equals(user.getActiveTrailElement()))
					itemIcon = ItemUtils.maybeAddGlow(itemIcon);

				Set<String> placeholders = StringUtils.getPlaceholders(itemIcon);
				itemIcon = getInstance().getReplacer().parse(itemIcon);

				callback.accept(itemIcon, placeholders, -1, call -> {
					clickOnTrail(player, user, trail, instance.getReplacer());
					instance.refreshOfType(GUIItemTypesGP.GP_TRAIL);
					instance.refreshOfType(GUIItemTypesGP.GP_TRAIL_REMOVE);
				});
			}
		};
	}

	public static void clickOnTrail(Player player, UserGP user, ElementTrail trail, Replacer replacer) {
		final Permission permission = trail.getPermission().directParseOrNull(replacer);
		if (permission != null && permission.has(player)) {
			if (trail.equals(user.getActiveTrailElement())) {
				user.setActiveTrail(null);
				TextGP.messageTrailDisable.send(player);
			} else {
				user.setActiveTrail(trail);
				TextGP.messageTrailEnable.replace("{trail}", () -> trail.getName().parse(replacer).orElse(trail.getId())).send(player);
			}
		} else
			TextGeneric.messageNoPermission.send(player);
	}

}
