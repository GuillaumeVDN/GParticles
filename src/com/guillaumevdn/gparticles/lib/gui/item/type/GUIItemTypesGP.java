package com.guillaumevdn.gparticles.lib.gui.item.type;

import com.guillaumevdn.gcore.lib.gui.element.item.ElementGUIItemType;
import com.guillaumevdn.gcore.lib.gui.element.item.type.GUIItemTypes;

/**
 * @author GuillaumeVDN
 */
public final class GUIItemTypesGP {

	private GUIItemTypesGP() {
	}

	public static TypeGUI 				GP_GUI;
	public static TypeGadget 			GP_GADGET;
	public static TypeParticle			GP_PARTICLE;
	public static TypeParticleRemove	GP_PARTICLE_REMOVE;
	public static TypeTrail				GP_TRAIL;
	public static TypeTrailRemove		GP_TRAIL_REMOVE;

	public static void init() {
		// reassign values here ; after a reload, this class will already have been initialized, so fields as well (and fields would have been registered in the old instance of GUIItemTypes.inst())
		GP_GUI				= GUIItemTypes.inst().register(new TypeGUI("GP_GUI"));
		GP_GADGET			= GUIItemTypes.inst().register(new TypeGadget("GP_GADGET"));
		GP_PARTICLE			= GUIItemTypes.inst().register(new TypeParticle("GP_PARTICLE"));
		GP_PARTICLE_REMOVE	= GUIItemTypes.inst().register(new TypeParticleRemove("GP_PARTICLE_REMOVE"));
		GP_TRAIL			= GUIItemTypes.inst().register(new TypeTrail("GP_TRAIL"));
		GP_TRAIL_REMOVE		= GUIItemTypes.inst().register(new TypeTrailRemove("GP_TRAIL_REMOVE"));

		// reset valuesCache of ElementGUIItemType ; an ElementGUIItem might already have been loaded, therefore its type as well, so all types would be cached
		ElementGUIItemType.valuesCache.clear();
	}

}
