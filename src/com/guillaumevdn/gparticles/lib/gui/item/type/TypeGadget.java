package com.guillaumevdn.gparticles.lib.gui.item.type;

import java.util.Set;
import java.util.function.Consumer;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.guillaumevdn.gcore.TextGeneric;
import com.guillaumevdn.gcore.lib.compatibility.material.CommonMats;
import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.compatibility.nbt.NBTItem;
import com.guillaumevdn.gcore.lib.element.struct.Need;
import com.guillaumevdn.gcore.lib.element.struct.parsing.ParsingError;
import com.guillaumevdn.gcore.lib.element.type.container.ElementItemMode;
import com.guillaumevdn.gcore.lib.function.QuadriConsumer;
import com.guillaumevdn.gcore.lib.gui.element.item.ElementGUIItem;
import com.guillaumevdn.gcore.lib.gui.element.item.type.GUIItemType;
import com.guillaumevdn.gcore.lib.gui.element.item.type.IconNeed;
import com.guillaumevdn.gcore.lib.gui.struct.ClickCall;
import com.guillaumevdn.gcore.lib.gui.struct.active.ActiveGUI;
import com.guillaumevdn.gcore.lib.gui.struct.active.ActiveItemHolder;
import com.guillaumevdn.gcore.lib.gui.struct.active.ActiveItemHolderElementGUIItem;
import com.guillaumevdn.gcore.lib.gui.struct.active.ActiveItemHolderElementGUIItemCommon;
import com.guillaumevdn.gcore.lib.gui.struct.active.ItemHolder;
import com.guillaumevdn.gcore.lib.item.ItemUtils;
import com.guillaumevdn.gcore.lib.number.NumberUtils;
import com.guillaumevdn.gcore.lib.permission.Permission;
import com.guillaumevdn.gcore.lib.string.StringUtils;
import com.guillaumevdn.gparticles.ConfigGP;
import com.guillaumevdn.gparticles.GParticles;
import com.guillaumevdn.gparticles.TextEditorGP;
import com.guillaumevdn.gparticles.TextGP;
import com.guillaumevdn.gparticles.data.user.UserGP;
import com.guillaumevdn.gparticles.lib.gadget.active.GadgetUtils;
import com.guillaumevdn.gparticles.lib.gadget.element.ElementGadget;
import com.guillaumevdn.gparticles.lib.gadget.element.ElementGadgetReference;

/**
 * @author GuillaumeVDN
 */
public class TypeGadget extends GUIItemType {

	public TypeGadget(String id) {
		super(id, IconNeed.REQUIRED, CommonMats.EMERALD);
	}

	@Override
	protected void doFillTypeSpecificElements(ElementGUIItem item) {
		super.doFillTypeSpecificElements(item);
		item.add(new ElementGadgetReference(item, "gadget", Need.required(), TextEditorGP.elementGUIItemTypeGadgetGadget));
		item.addItem("icon_no_permission", Need.optional(), ElementItemMode.BUILDABLE, TextEditorGP.elementGUIItemTypeGadgetIconNoPermission);
		item.addItem("icon_hotbar", Need.optional(), ElementItemMode.BUILDABLE, TextEditorGP.elementGUIItemTypeGadgetIconNoPermission);
	}

	@Override
	public ActiveItemHolder newActive(ActiveGUI instance, ItemHolder holder, ElementGUIItem element) {
		return new ActiveItemHolderElementGUIItemCommon(instance, holder, element) {
			@Override
			protected void build(ItemStack itemIcon, QuadriConsumer<ItemStack, Set<String>, Integer, Consumer<ClickCall>> callback) throws ParsingError {

				final Player player = instance.getReplacer().getReplacerData().getPlayer();
				final UserGP user = UserGP.cachedOrNull(player);
				if (user == null) {
					return;
				}

				final ElementGadget gadget = element.directParseNoCatchOrThrowParsingNull("gadget", instance.getReplacer());
				final Permission permission = gadget.getPermission().directParseOrNull(instance.getReplacer());

				if (permission != null && !permission.has(player)) {
					itemIcon = element.directParseOrNull("icon_no_permission", instance.getReplacer());
					if (itemIcon == null) {
						return;
					}
					itemIcon = ActiveItemHolderElementGUIItem.resetNameLorePlaceholders(itemIcon, element.getElementAs("icon_no_permission"));
				}

				final ItemStack hotbarItem = element.directParseOrNull("icon_hotbar", instance.getReplacer());

				final Set<String> placeholders = StringUtils.getPlaceholders(itemIcon);
				itemIcon = getInstance().getReplacer().parse(itemIcon);

				callback.accept(itemIcon, placeholders, -1, call -> {
					if (permission != null && permission.has(player)) {
						// set in hotbar
						if (NumberUtils.isInRange(ConfigGP.hotbarSlotGadget, 0, 8) && hotbarItem != null) {

							// build nbt
							final ItemStack hotbar;
							try {
								final NBTItem nbt = new NBTItem(hotbarItem, true);
								nbt.setString("GParticlesGadget", gadget.getType().getId());
								hotbar = nbt.getModifiedItem();
							} catch (Throwable error) {
								GParticles.inst().getMainLogger().error("Couldn't set hotbar NBT item " + gadget.getType(), error);
								return;
							}

							// replace existing item
							final ItemStack existing = player.getInventory().getItem(ConfigGP.hotbarSlotGadget);
							player.getInventory().setItem(ConfigGP.hotbarSlotGadget, hotbar);

							if (Mat.fromItem(existing).orAir().isAir() || GadgetUtils.isGadgetItem(existing)) {
								player.updateInventory();
							} else {
								ItemUtils.give(player, existing, true);
							}

							// notify and close inventory
							TextGP.messageGadgetSetHotbar.send(call);
							player.closeInventory();
							player.getInventory().setHeldItemSlot(ConfigGP.hotbarSlotGadget);
						}
						// start directly
						else {
							gadget.getType().attemptStart(gadget, player);
						}
					} else {
						TextGeneric.messageNoPermission.send(call);
					}
				});
			}
		};
	}

}
