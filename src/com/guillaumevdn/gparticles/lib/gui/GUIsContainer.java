package com.guillaumevdn.gparticles.lib.gui;

import java.io.File;

import com.guillaumevdn.gcore.lib.element.struct.list.referenceable.ElementsContainer;
import com.guillaumevdn.gparticles.GParticles;

/**
 * @author GuillaumeVDN
 */
public class GUIsContainer extends ElementsContainer<ElementGUIGP> {

	public GUIsContainer() {
		super(GParticles.inst(), "GUI", ElementGUIGP.class, GParticles.inst().getDataFile("guis/"));
	}

	@Override
	protected ElementGUIGP createElement(File file, String id) {
		return new ElementGUIGP(file, id);
	}

}
