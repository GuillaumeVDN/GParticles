package com.guillaumevdn.gparticles.lib.gadget.types;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.DyeColor;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.entity.Sheep;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import com.guillaumevdn.gcore.lib.collection.CollectionUtils;
import com.guillaumevdn.gcore.lib.compatibility.material.CommonMats;
import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.compatibility.sound.Sound;
import com.guillaumevdn.gcore.lib.element.struct.Need;
import com.guillaumevdn.gcore.lib.element.struct.parsing.ParsingError;
import com.guillaumevdn.gcore.lib.number.NumberUtils;
import com.guillaumevdn.gcore.lib.string.StringUtils;
import com.guillaumevdn.gcore.lib.string.placeholder.Replacer;
import com.guillaumevdn.gparticles.GParticles;
import com.guillaumevdn.gparticles.lib.gadget.active.ActiveGadget;
import com.guillaumevdn.gparticles.lib.gadget.element.ElementGadget;
import com.guillaumevdn.gparticles.lib.gadget.element.GadgetType;

/**
 * @author GuillaumeVDN
 */
public final class GadgetTypeDiscoSheep extends GadgetType {

	private static final List<ItemStack> WOOL_ITEMS = Stream
			.of("BLACK_WOOL", "BLUE_WOOL", "BROWN_WOOL", "CYAN_WOOL", "GRAY_WOOL", "GREEN_WOOL", "LIGHT_BLUE_WOOL", "LIGHT_GRAY_WOOL", "LIME_WOOL", "MAGENTA_WOOL", "ORANGE_WOOL", "PINK_WOOL", "PURPLE_WOOL", "RED_WOOL", "WHITE_WOOL", "YELLOW_WOOL")
			.map(name -> Mat.firstFromIdOrDataName(name).orAir())
			.filter(mat -> !mat.isAir())
			.map(mat -> mat.newStack())
			.collect(Collectors.toList());

	public GadgetTypeDiscoSheep(String id) {
		super(id, CommonMats.BEDROCK);
	}

	@Override
	protected void doFillTypeSpecificElements(ElementGadget gadget) {
		super.doFillTypeSpecificElements(gadget);
		gadget.addDuration("duration", Need.required(), null, null, null);
		gadget.addDuration("switch_delay", Need.required(), null, null, null);
		gadget.addSound("sound_switch", Need.optional(), null);
	}

	@Override
	public ActiveGadget create(ElementGadget gadget, Player player) throws ParsingError {
		final Replacer rep = Replacer.of(player);

		final int durationTicks = (int) ((long) gadget.directParseNoCatchOrThrowParsingNull("duration", rep) / 50L);
		final int switchDelayTicks = (int) ((long) gadget.directParseNoCatchOrThrowParsingNull("switch_delay", rep) / 50L);
		final Sound soundSwitch = gadget.directParseOrNull("sound_switch", rep);

		return new ActiveGadget(gadget, player) {

			private final String id = "gadget_" + player.getName() + "_" + StringUtils.generateRandomAlphanumericString(10);

			private int remainingTicks = durationTicks;
			private int ticksBeforeSwitch = 0;
			private Sheep sheep;
			private List<Item> items = new ArrayList<>();

			@Override
			protected void doStart() {
				sheep = (Sheep) player.getWorld().spawnEntity(player.getLocation(), EntityType.SHEEP);

				GParticles.inst().registerTask(id, false, 1, () -> tick());
				GParticles.inst().registerListener(id, this);
			}

			private void tick() {
				// stop
				if (--remainingTicks <= 0) {
					stop();
					return;
				}

				// switch
				if (--ticksBeforeSwitch <= 0) {
					ticksBeforeSwitch = switchDelayTicks;

					// set color
					sheep.setColor(CollectionUtils.randomArray(DyeColor.values()));

					// spawn item
					Item item = sheep.getWorld().dropItem(sheep.getLocation().clone().add(0d, 1d, 0d), CollectionUtils.random(WOOL_ITEMS));
					item.setPickupDelay(Integer.MAX_VALUE);
					item.setVelocity(new Vector(NumberUtils.random(-0.2d, 0.2d), NumberUtils.random(0.2d, 0.5d), NumberUtils.random(-0.2d, 0.2d)));
					items.add(item);

					// sound
					if (soundSwitch != null) {
						soundSwitch.play(sheep.getLocation());
					}
				}
			}

			@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
			public void event(EntityDamageEvent event) {
				if (event.getEntity().equals(sheep)) {
					event.setDamage(0d);
					event.setCancelled(true);
				}
			}

			@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
			public void event(EntityCombustEvent event) {
				if (event.getEntity().equals(sheep)) {
					event.setCancelled(true);
				}
			}

			@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
			public void onHopperHop(InventoryPickupItemEvent event) {
				if (items.contains(event.getItem())) {
					event.setCancelled(true);
				}
			}

			@Override
			protected void doStop() {
				GParticles.inst().stopTask(id);
				GParticles.inst().stopListener(id);

				sheep.remove();
				items.forEach(it -> it.remove());
			}

		};
	}

}
