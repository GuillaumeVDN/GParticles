package com.guillaumevdn.gparticles.lib.gadget.types;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcore.lib.collection.CollectionUtils;
import com.guillaumevdn.gcore.lib.compatibility.material.CommonMats;
import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.configuration.FakeSuperElement;
import com.guillaumevdn.gcore.lib.element.struct.Need;
import com.guillaumevdn.gcore.lib.element.struct.parsing.ParsingError;
import com.guillaumevdn.gcore.lib.string.StringUtils;
import com.guillaumevdn.gcore.lib.string.placeholder.Replacer;
import com.guillaumevdn.gparticles.GParticles;
import com.guillaumevdn.gparticles.data.user.UserGP;
import com.guillaumevdn.gparticles.lib.gadget.active.ActiveGadget;
import com.guillaumevdn.gparticles.lib.gadget.element.ElementGadget;
import com.guillaumevdn.gparticles.lib.gadget.element.GadgetType;
import com.guillaumevdn.gparticles.lib.particle.ElementParticleGP;
import com.guillaumevdn.gparticles.lib.particle.displayer.active.ParticleRunner;
import com.guillaumevdn.gparticles.lib.particle.displayer.active.ParticleRunnerRandomOffset;
import com.guillaumevdn.gparticles.lib.particle.displayer.element.particle.ElementDisplayerParticle;
import com.guillaumevdn.gparticles.lib.particle.displayer.element.particle.ElementDisplayerParticleList;
import com.guillaumevdn.gparticles.lib.trail.active.TrailRunner;
import com.guillaumevdn.gparticles.lib.trail.element.ElementTrail;

/**
 * @author GuillaumeVDN
 */
public final class GadgetTypePyromaniac extends GadgetType {

	private static final List<Mat> TRAIL_BLOCK_TYPES = Stream
			.of("NETHERRACK", "NETHER_BRICKS", "MAGMA_BLOCK")
			.map(name -> Mat.firstFromIdOrDataName(name).orAir())
			.filter(mat -> !mat.isAir())
			.collect(Collectors.toList());

	private static final ElementDisplayerParticleList PARTICLES = new ElementDisplayerParticleList(new FakeSuperElement(GParticles.inst()), "particles", Need.optional(), null); {
		final ElementDisplayerParticle particle = new ElementDisplayerParticle(PARTICLES, "lava", Need.optional(), null);
		particle.getParticle().setValue(CollectionUtils.asList("LAVA"));
		PARTICLES.add(particle);
	}

	public GadgetTypePyromaniac(String id) {
		super(id, CommonMats.BEDROCK);
	}

	@Override
	protected void doFillTypeSpecificElements(ElementGadget gadget) {
		super.doFillTypeSpecificElements(gadget);
		gadget.addDuration("duration", Need.required(), null, null, null);
		gadget.addDuration("block_change_delay", Need.required(), null, null, null);
		gadget.addDuration("persistence_time", Need.required(), null, null, null);
	}

	@Override
	public ActiveGadget create(ElementGadget gadget, Player player) throws ParsingError {
		final Replacer rep = Replacer.of(player);
		final int duration = (int) ((long) gadget.directParseNoCatchOrThrowParsingNull("duration", rep) / 50L);

		return new ActiveGadget(gadget, player) {

			private final String id = "gadget_" + player.getName() + "_" + StringUtils.generateRandomAlphanumericString(10);

			private ElementParticleGP ogParticles = null;
			private ElementTrail ogTrail = null;

			private int remainingTicks = duration;
			private TrailRunner trailRunner = new TrailRunner(player, TRAIL_BLOCK_TYPES, 0, 2, 5000L);
			private ParticleRunner particleRunner = new ParticleRunnerRandomOffset(player, -0.5d, 0.5d, -0.5d, 0.5d, 0d, 2d, 2, 1, PARTICLES);

			@Override
			protected void doStart() {
				final UserGP user = UserGP.cachedOrNull(player);
				if (user != null) {
					ogParticles = user.getActiveParticleElement();
					ogTrail = user.getActiveTrailElement();

					user.setActiveParticle(null);
					user.setActiveTrail(null);
				}

				GParticles.inst().registerTask(id, false, 1, () -> tick());
				GParticles.inst().registerListener(id, this);
			}

			private void tick() throws Throwable {
				// done
				if (--remainingTicks <= 0) {
					stop();
					return;
				}

				// tick trail and particle
				trailRunner.run();
				particleRunner.run();
			}

			@Override
			protected void doStop() {
				GParticles.inst().stopTask(id);
				GParticles.inst().stopListener(id);

				trailRunner.cancel();

				final UserGP user = UserGP.cachedOrNull(player);
				if (user != null) {
					if (ogParticles != null) {
						user.setActiveParticle(ogParticles);
					}
					if (ogTrail != null) {
						user.setActiveTrail(ogTrail);
					}
				}
			}

		};
	}

}
