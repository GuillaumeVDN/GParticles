package com.guillaumevdn.gparticles.lib.gadget.types;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.hanging.HangingBreakEvent.RemoveCause;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import com.guillaumevdn.gcore.lib.collection.CollectionUtils;
import com.guillaumevdn.gcore.lib.compatibility.Version;
import com.guillaumevdn.gcore.lib.compatibility.material.CommonMats;
import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.compatibility.sound.Sound;
import com.guillaumevdn.gcore.lib.element.struct.Need;
import com.guillaumevdn.gcore.lib.element.struct.parsing.ParsingError;
import com.guillaumevdn.gcore.lib.number.NumberUtils;
import com.guillaumevdn.gcore.lib.object.ObjectUtils;
import com.guillaumevdn.gcore.lib.string.StringUtils;
import com.guillaumevdn.gcore.lib.string.placeholder.Replacer;
import com.guillaumevdn.gparticles.GParticles;
import com.guillaumevdn.gparticles.lib.gadget.active.ActiveGadget;
import com.guillaumevdn.gparticles.lib.gadget.element.ElementGadget;
import com.guillaumevdn.gparticles.lib.gadget.element.GadgetType;

/**
 * @author GuillaumeVDN
 */
public final class GadgetTypeMobDance extends GadgetType {

	private static final List<ItemStack> DROP_ITEMS = Stream
			.of("BONE_MEAL", "RED_DYE", "CREEPER_SPAWN_EGG", "CREEPER_HEAD", "GUNPOWDER", "BONE")
			.map(name -> Mat.firstFromIdOrDataName(name).orAir())
			.filter(mat -> !mat.isAir())
			.map(mat -> mat.newStack())
			.collect(Collectors.toList());

	private static final List<EntityType> MOB_TYPES = Stream
			.of("CREEPER", "SKELETON", "ZOMBIE", Version.ATLEAST_1_16 ? "ZOMBIFIED_PIGLIN" : "PIG_ZOMBIE")
			.map(name -> ObjectUtils.safeValueOf(name, EntityType.class))
			.filter(ent -> ent != null)
			.collect(Collectors.toList());

	private static final Sound SOUND_EXPLODE = Sound.firstFromIdOrDataName("ENTITY_GENERIC_EXPLODE").orNull();

	public GadgetTypeMobDance(String id) {
		super(id, CommonMats.BEDROCK);
	}

	@Override
	protected void doFillTypeSpecificElements(ElementGadget gadget) {
		super.doFillTypeSpecificElements(gadget);
		gadget.addDuration("duration", Need.required(), null, null, null);
		gadget.addDuration("rotate_delay", Need.required(), null, null, null);
		gadget.addDouble("rotation_speed", Need.required(), null, null);
		gadget.addDouble("radius", Need.required(), 0d, null, null);
		gadget.addInteger("mob_count", Need.required(), 1, null, null);
		gadget.addInteger("item_spawn_count", Need.optional(), 0, null, null);
		gadget.addChancePercentage("item_spawn_chance", Need.required(), null);
		gadget.addChancePercentage("tnt_spawn_chance", Need.required(), null);
		gadget.addMatList("discs", Need.optional(), null);
	}

	@Override
	public ActiveGadget create(ElementGadget gadget, Player player) throws ParsingError {
		final Replacer rep = Replacer.of(player);

		final int durationTicks = (int) ((long) gadget.directParseNoCatchOrThrowParsingNull("duration", rep) / 50L);
		final int rotateDelayTicks = (int) ((long) gadget.directParseNoCatchOrThrowParsingNull("rotate_delay", rep) / 50L);
		final double rotationSpeed = gadget.directParseNoCatchOrThrowParsingNull("rotation_speed", rep);
		final double radius = gadget.directParseNoCatchOrThrowParsingNull("radius", rep);
		final int mobCount = gadget.directParseNoCatchOrThrowParsingNull("mob_count", rep);
		final int itemSpawnCount = gadget.directParseOrElse("item_spawn_count", rep, 0);
		final double itemSpawnChance = gadget.directParseNoCatchOrThrowParsingNull("item_spawn_chance", rep);
		final double tntSpawnChance = gadget.directParseNoCatchOrThrowParsingNull("tnt_spawn_chance", rep);
		final List<Mat> discs = gadget.parseElementAsList("discs", Mat.class, rep).orEmptyList();

		return new ActiveGadget(gadget, player) {

			private final String id = "gadget_" + player.getName() + "_" + StringUtils.generateRandomAlphanumericString(10);
			private final Location base = player.getLocation();
			private double rotation = 0d;

			private int remainingTicks = durationTicks;
			private int ticksBeforeRotate = rotateDelayTicks;
			private List<Entity> mobs = new ArrayList<>();
			private List<TNTPrimed> tnts = new ArrayList<>();
			private List<Item> items = new ArrayList<>();

			@Override
			protected void doStart() {

				// spawn mobs
				final double angle = Math.toRadians(360d / (double) mobCount);
				double at = 0d;
				for (int i = 0; i < mobCount; i++) {
					final double x = Math.cos(at) * radius;
					final double z = Math.sin(at) * radius;
					at += angle;

					final Entity mob = base.getWorld().spawnEntity(base.clone().add(x, 0d, z), CollectionUtils.random(MOB_TYPES));
					if (mob.getType().equals(EntityType.CREEPER) && NumberUtils.random()) {
						((Creeper) mob).setPowered(true);
					}
					mobs.add(mob);
				}

				// play disc
				try {
					final Mat disc = CollectionUtils.random(discs);
					if (disc != null) {
						base.getWorld().playEffect(base, Effect.RECORD_PLAY, disc.getData().getDataInstance());
					}
				} catch (Throwable exception) {
					exception.printStackTrace();
					GParticles.inst().getMainLogger().error("Couldn't play disc for the Disco Box gadget");
				}

				// start task and listener
				GParticles.inst().registerTask(id, false, 1, () -> tick());
				GParticles.inst().registerListener(id, this);
			}

			private void tick() {
				// stop
				if (--remainingTicks <= 0) {
					stop();
					return;
				}

				// rotate
				if (--ticksBeforeRotate <= 0) {
					ticksBeforeRotate = rotateDelayTicks;

					// rotate mobs
					rotation += rotationSpeed;

					final double angle = Math.toRadians(360d / (double) mobCount);
					double at = Math.toRadians(rotation);
					for (int i = 0; i < mobCount; i++) {
						final double x = Math.cos(at) * radius;
						final double z = Math.sin(at) * radius;
						at += angle;

						mobs.get(i).teleport(base.clone().add(x, 0d, z));
					}

					// spawn tnt
					if (NumberUtils.random(0d, 100d) <= tntSpawnChance) {
						TNTPrimed tnt = (TNTPrimed) base.getWorld().spawnEntity(base, EntityType.PRIMED_TNT);
						tnt.setVelocity(new Vector(NumberUtils.random(-1.5d, 1.5d), NumberUtils.random(0d, 1.5d), NumberUtils.random(-1.5d, 1.5d)));
						mobs.add(tnt);
					}

					// spawn items
					if (NumberUtils.random(0d, 100d) <= itemSpawnChance) {
						for (int i = 0; i < itemSpawnCount; i++) {
							Item item = base.getWorld().dropItem(base.clone().add(0d, 1d, 0d), CollectionUtils.random(DROP_ITEMS));
							item.setPickupDelay(Integer.MAX_VALUE);
							item.setVelocity(new Vector(NumberUtils.random(-1d, 1d), NumberUtils.random(0d, 1d), NumberUtils.random(-1d, 1d)));
							items.add(item);
						}
					}
				}
			}

			@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
			public void event(EntityDamageEvent event) {
				if (mobs.contains(event.getEntity())) {
					event.setDamage(0d);
					event.setCancelled(true);
				}
			}

			@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
			public void event(EntityCombustEvent event) {
				if (mobs.contains(event.getEntity())) {
					event.setCancelled(true);
				}
			}

			@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
			public void event(InventoryPickupItemEvent event) {
				if (items.contains(event.getItem())) {
					event.setCancelled(true);
				}
			}

			@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
			public void event(EntityDamageByEntityEvent event) {
				if (tnts.contains(event.getDamager())) {
					event.setCancelled(true);
				}
			}

			@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
			public void event(HangingBreakEvent event) {
				if (event.getCause().equals(RemoveCause.EXPLOSION) && tnts.stream().anyMatch(tnt -> event.getEntity().getLocation().distance(tnt.getLocation()) <= 30d)) {
					event.setCancelled(true);
				}
			}

			@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
			public void event(EntityExplodeEvent event) {
				if (mobs.contains(event.getEntity())) {
					event.setCancelled(true);
				} else if (tnts.contains(event.getEntity())) {
					event.setCancelled(true);

					// remove from list
					TNTPrimed tnt = (TNTPrimed) event.getEntity();
					tnts.remove(tnt);
					tnt.remove();

					// sound
					if (SOUND_EXPLODE != null) {
						SOUND_EXPLODE.play(tnt.getLocation());
					}

					// items
					for (int i = 0; i < itemSpawnChance; i++) {
						Item item = tnt.getWorld().dropItem(tnt.getLocation(), CollectionUtils.random(DROP_ITEMS));
						item.setPickupDelay(Integer.MAX_VALUE);
						item.setVelocity(new Vector(NumberUtils.random(-1d, 1d), NumberUtils.random(0d, 1d), NumberUtils.random(-1d, 1d)));
						items.add(item);
					}
				}
			}

			@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
			public void event(EntityTargetEvent event) {
				if (mobs.contains(event.getEntity())) {
					event.setCancelled(true);
				}
			}

			@Override
			protected void doStop() {
				GParticles.inst().stopTask(id);
				GParticles.inst().stopListener(id);

				base.getWorld().playEffect(base, Effect.RECORD_PLAY, 0);

				mobs.forEach(mob -> mob.remove());
				tnts.forEach(tnt -> tnt.remove());
				items.forEach(it -> it.remove());
			}

		};
	}

}
