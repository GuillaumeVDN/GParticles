package com.guillaumevdn.gparticles.lib.gadget.types;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.block.Block;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import com.guillaumevdn.gcore.lib.compatibility.material.CommonMats;
import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.compatibility.sound.Sound;
import com.guillaumevdn.gcore.lib.element.struct.Need;
import com.guillaumevdn.gcore.lib.element.struct.parsing.ParsingError;
import com.guillaumevdn.gcore.lib.number.NumberUtils;
import com.guillaumevdn.gcore.lib.string.StringUtils;
import com.guillaumevdn.gcore.lib.string.placeholder.Replacer;
import com.guillaumevdn.gparticles.GParticles;
import com.guillaumevdn.gparticles.lib.gadget.active.ActiveGadget;
import com.guillaumevdn.gparticles.lib.gadget.element.ElementGadget;
import com.guillaumevdn.gparticles.lib.gadget.element.GadgetType;

/**
 * @author GuillaumeVDN
 */
public final class GadgetTypeCocoaBomb extends GadgetType {

	private static final ItemStack ITEM_COCOA_BEANS = CommonMats.COCOA_BEANS.newStack();

	public GadgetTypeCocoaBomb(String id) {
		super(id, CommonMats.BEDROCK);
	}

	@Override
	protected void doFillTypeSpecificElements(ElementGadget gadget) {
		super.doFillTypeSpecificElements(gadget);
		gadget.addDuration("explosion_time", Need.required(), null, null, null);
		gadget.addDuration("cleanup_delay", Need.required(), null, null, null);
		gadget.addDuration("switch_delay", Need.required(), null, null, null);
		gadget.addInteger("item_count", Need.required(), 0, null);
		gadget.addSound("sound_initiate", Need.optional(), null);
		gadget.addSound("sound_switch", Need.optional(), null);
		gadget.addSound("sound_explode", Need.optional(), null);
	}

	@Override
	public ActiveGadget create(ElementGadget gadget, Player player) throws ParsingError {
		final Replacer rep = Replacer.of(player);

		final int explosionTicks = (int) ((long) gadget.directParseNoCatchOrThrowParsingNull("explosion_time", rep) / 50L);
		final int cleanupTicks = (int) ((long) gadget.directParseNoCatchOrThrowParsingNull("cleanup_delay", rep) / 50L);
		final int switchTicks = (int) ((long) gadget.directParseNoCatchOrThrowParsingNull("switch_delay", rep) / 50L);
		final int itemCount = gadget.directParseOrElse("item_count", rep, 0);
		final Sound soundInitiate = gadget.directParseOrNull("sound_initiate", rep);
		final Sound soundSwitch = gadget.directParseOrNull("sound_switch", rep);
		final Sound soundExplode = gadget.directParseOrNull("sound_explode", rep);

		return new ActiveGadget(gadget, player) {

			private final Block block = player.getLocation().getBlock();
			private final String id = "gadget_" + player.getName() + "_" + StringUtils.generateRandomAlphanumericString(10);

			private int remainingTicks = explosionTicks;
			private int ticksBeforeSwitch = switchTicks;
			private boolean bombBrown = true;
			private boolean bombExploded = false;
			private List<Item> items = new ArrayList<>();

			@Override
			protected void doStart() {
				CommonMats.BROWN_WOOL.setBlockChange(block);
				if (soundInitiate != null) {
					soundInitiate.play(block.getWorld().getPlayers(), block.getLocation());
				}

				GParticles.inst().registerTask(id, false, 1, () -> tick());
				GParticles.inst().registerListener(id, this);
			}

			private void tick() {
				// clear items
				if (bombExploded) {
					if (--remainingTicks <= 0) {
						stop();
					}
					return;
				}

				// explode
				if (--remainingTicks <= 0) {
					bombExploded = true;
					remainingTicks = cleanupTicks;

					// reset block
					final Mat mat = Mat.fromBlock(block).orAir();
					mat.setBlockChange(block);

					// play sound
					if (soundExplode != null) {
						soundExplode.play(block.getWorld().getPlayers(), block.getLocation());
					}

					// spawn items
					for (int i = 0; i < itemCount; i++) {
						final Item item = block.getWorld().dropItem(block.getLocation(), ITEM_COCOA_BEANS);
						item.setPickupDelay(Integer.MAX_VALUE);
						item.setVelocity(new Vector(NumberUtils.random(-1.0D, 1.0D), NumberUtils.random(0.0D, 1.0D), NumberUtils.random(-1.0D, 1.0D)));
						items.add(item);
					}
				}
				// switch type
				else if (--ticksBeforeSwitch <= 0) {
					ticksBeforeSwitch = switchTicks;

					final Mat mat = bombBrown ? CommonMats.WHITE_WOOL : CommonMats.BROWN_WOOL;
					mat.setBlockChange(block);
					bombBrown = !bombBrown;

					if (soundSwitch != null) {
						soundSwitch.play(block.getWorld().getPlayers(), block.getLocation());
					}
				}

			}

			@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
			public void onHopperHop(InventoryPickupItemEvent event) {
				if (items.contains(event.getItem())) {
					event.setCancelled(true);
				}
			}

			@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
			public void event(BlockBreakEvent event) {
				final Block block = event.getBlock();
				if (block.equals(this.block)) {
					event.setCancelled(true);
				}
			}

			@Override
			protected void doStop() {
				GParticles.inst().stopTask(id);
				GParticles.inst().stopListener(id);

				if (bombExploded) {
					items.forEach(it -> it.remove());
					items.clear();
				} else {
					final Mat mat = Mat.fromBlock(block).orAir();
					mat.setBlockChange(block);
				}
			}

		};
	}

}
