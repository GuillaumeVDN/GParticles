package com.guillaumevdn.gparticles.lib.gadget.active;

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import com.guillaumevdn.gparticles.GParticles;
import com.guillaumevdn.gparticles.data.user.UserGP;
import com.guillaumevdn.gparticles.lib.gadget.element.ElementGadget;

public abstract class ActiveGadget implements Listener {

	private final ElementGadget gadget;
	private final Player player;

	protected ActiveGadget(ElementGadget gadget, Player player) {
		this.gadget = gadget;
		this.player = player;
	}

	public ElementGadget getGadget() {
		return gadget;
	}

	public Player getPlayer() {
		return player;
	}

	public final void start() {
		GParticles.inst().getActiveGadgets().put(player, this);
		doStart();
	}

	public final void stop() {
		stop(true);
	}

	public final void stop(boolean unregister) {
		doStop();
		if (unregister) {
			GParticles.inst().getActiveGadgets().remove(player);
		}

		UserGP.forCached(player, user -> {
			user.setLastGadgetUse(gadget.getType(), System.currentTimeMillis());
		});
	}

	protected abstract void doStart();
	protected abstract void doStop();

}
