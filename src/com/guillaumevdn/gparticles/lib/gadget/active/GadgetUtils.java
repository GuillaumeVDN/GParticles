
package com.guillaumevdn.gparticles.lib.gadget.active;

import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;

import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.compatibility.nbt.NBTItem;
import com.guillaumevdn.gparticles.ConfigGP;
import com.guillaumevdn.gparticles.GParticles;
import com.guillaumevdn.gparticles.lib.gadget.element.GadgetType;
import com.guillaumevdn.gparticles.lib.gadget.element.GadgetTypes;

/**
 * @author GuillaumeVDN
 */
public class GadgetUtils {

	public static final String NBT_TAG_GADGET_TYPE = "GParticlesGadget";

	public static boolean isGadgetItem(ItemStack item) {
		try {
			final NBTItem nbt = new NBTItem(item, false);
			return nbt.hasKey(NBT_TAG_GADGET_TYPE);
		} catch (Throwable error) {
			GParticles.inst().getMainLogger().error("Couldn't check NBT item", error);
		}
		return false;
	}

	public static GadgetType getGadgetType(ItemStack item) {
		try {
			final NBTItem nbt = new NBTItem(item, false);
			if (nbt.hasKey(NBT_TAG_GADGET_TYPE)) {
				return GadgetTypes.inst().safeValueOf(nbt.getString(NBT_TAG_GADGET_TYPE));
			}
		} catch (Throwable error) {
			GParticles.inst().getMainLogger().error("Couldn't check NBT item", error);
		}
		return null;
	}

	public static boolean mustIgnoreBlock(Block block) {
		return mustIgnoreBlock(Mat.fromBlock(block).orAir());
	}

	public static boolean mustIgnoreBlock(Mat blockType) {
		return blockType.isAir()
				|| blockType.isWater()
				|| blockType.getData().isTraversable()
				|| blockType.getData().isDoor()
				|| ConfigGP.trailBlocksBlacklist.stream().anyMatch(ignore -> blockType.getData().getDataName().contains(ignore));
	}

	public static boolean mustIgnoreBlockDiscoBox(Block block) {
		return mustIgnoreBlockDiscoBox(Mat.fromBlock(block).orAir());
	}

	public static boolean mustIgnoreBlockDiscoBox(Mat blockType) {
		return blockType.isWater()
				|| blockType.getData().isDoor()
				|| ConfigGP.trailBlocksBlacklist.stream().anyMatch(ignore -> blockType.getData().getDataName().contains(ignore));
	}

}
