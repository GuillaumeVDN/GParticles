package com.guillaumevdn.gparticles.lib.gadget.element;

import java.util.Comparator;
import java.util.List;

import com.guillaumevdn.gcore.lib.compatibility.material.CommonMats;
import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.concurrency.RWWeakHashMap;
import com.guillaumevdn.gcore.lib.element.struct.Element;
import com.guillaumevdn.gcore.lib.element.struct.container.typable.ElementTypableElementType;
import com.guillaumevdn.gcore.lib.string.Text;

/**
 * @author GuillaumeVDN
 */
public final class ElementGadgetType extends ElementTypableElementType<GadgetType> {

	public ElementGadgetType(Element parent, String id, Text editorDescription) {
		super(GadgetTypes.inst(), parent, id, editorDescription);
	}

	private static RWWeakHashMap<Object, List<GadgetType>> cache = new RWWeakHashMap<>(1, 1f);
	@Override
	protected List<GadgetType> cacheOrBuild() {
		return cachedOrBuild(cache, () -> GadgetTypes.inst().values().stream().sorted(Comparator.comparing(e -> e.getId())));
	}

	@Override
	public Mat editorIconType() {
		return parseGeneric().ifPresentMap(GadgetType::getIcon).orElse(CommonMats.REDSTONE);
	}

}
