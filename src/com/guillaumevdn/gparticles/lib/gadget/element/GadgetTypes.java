package com.guillaumevdn.gparticles.lib.gadget.element;

import com.guillaumevdn.gcore.lib.element.struct.container.typable.TypableElementTypes;
import com.guillaumevdn.gparticles.GParticles;
import com.guillaumevdn.gparticles.lib.gadget.types.GadgetTypeCocoaBomb;
import com.guillaumevdn.gparticles.lib.gadget.types.GadgetTypeColorGun;
import com.guillaumevdn.gparticles.lib.gadget.types.GadgetTypeDiscoBox;
import com.guillaumevdn.gparticles.lib.gadget.types.GadgetTypeDiscoSheep;
import com.guillaumevdn.gparticles.lib.gadget.types.GadgetTypeMobDance;
import com.guillaumevdn.gparticles.lib.gadget.types.GadgetTypePigFountain;
import com.guillaumevdn.gparticles.lib.gadget.types.GadgetTypePyromaniac;

/**
 * @author GuillaumeVDN
 */
public final class GadgetTypes extends TypableElementTypes<GadgetType> {

	public GadgetTypes() {
		super(GadgetType.class);
	}

	// ----- types
	public final GadgetTypeCocoaBomb		COCOA_BOMB			= register(new GadgetTypeCocoaBomb("COCOA_BOMB"));
	public final GadgetTypeColorGun			COLOR_GUN			= register(new GadgetTypeColorGun("COLOR_GUN"));
	public final GadgetTypeDiscoBox			DISCO_BOX			= register(new GadgetTypeDiscoBox("DISCO_BOX"));
	public final GadgetTypeDiscoSheep		DISCO_SHEEP			= register(new GadgetTypeDiscoSheep("DISCO_SHEEP"));
	public final GadgetTypeMobDance			MOB_DANCE			= register(new GadgetTypeMobDance("MOB_DANCE"));
	public final GadgetTypePigFountain		PIG_FOUNTAIN		= register(new GadgetTypePigFountain("PIG_FOUNTAIN"));
	public final GadgetTypePyromaniac		PYROMANIAC			= register(new GadgetTypePyromaniac("PYROMANIAC"));

	// ----- values
	public static GadgetTypes inst() {
		return GParticles.inst().getGadgetTypes();
	}

	@Override
	public GadgetType defaultValue() {
		return COCOA_BOMB;
	}

}
