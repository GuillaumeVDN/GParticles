package com.guillaumevdn.gparticles.lib.command;

import java.util.List;
import java.util.stream.Collectors;

import com.guillaumevdn.gcore.lib.command.CommandCall;
import com.guillaumevdn.gcore.lib.command.argument.Argument;
import com.guillaumevdn.gcore.lib.object.NeedType;
import com.guillaumevdn.gcore.lib.permission.Permission;
import com.guillaumevdn.gcore.lib.string.Text;
import com.guillaumevdn.gparticles.ConfigGP;
import com.guillaumevdn.gparticles.lib.trail.element.ElementTrail;

/**
 * @author GuillaumeVDN
 */
public class ArgumentTrail extends Argument<ElementTrail> {

	public ArgumentTrail(NeedType need, boolean playerOnly, Permission permission, Text usage) {
		super(need, playerOnly, permission, usage);
	}

	@Override
	public ElementTrail consume(CommandCall call) {
		if (call.getArguments().isEmpty())
			return null;
		for (int i = 0; i < call.getArguments().size(); ++i) {
			final ElementTrail trail = ConfigGP.trails.getElement(call.getArguments().get(i).toLowerCase()).orNull();
			if (trail != null) {
				call.getArguments().remove(i);
				return trail;
			}
		}
		return null;
	}

	@Override
	public List<String> tabComplete(CommandCall call) {
		return ConfigGP.trails.values().stream().map(ElementTrail::getId).sorted(String::compareTo).collect(Collectors.toList());
	}

}
