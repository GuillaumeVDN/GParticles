package com.guillaumevdn.gparticles.lib.command;

import java.util.List;
import java.util.stream.Collectors;

import com.guillaumevdn.gcore.lib.command.CommandCall;
import com.guillaumevdn.gcore.lib.command.argument.Argument;
import com.guillaumevdn.gcore.lib.object.NeedType;
import com.guillaumevdn.gcore.lib.permission.Permission;
import com.guillaumevdn.gcore.lib.string.Text;
import com.guillaumevdn.gparticles.lib.gadget.element.GadgetType;
import com.guillaumevdn.gparticles.lib.gadget.element.GadgetTypes;

/**
 * @author GuillaumeVDN
 */
public class ArgumentGadgetType extends Argument<GadgetType> {

	public ArgumentGadgetType(NeedType need, boolean playerOnly, Permission permission, Text usage) {
		super(need, playerOnly, permission, usage);
	}

	@Override
	public GadgetType consume(CommandCall call) {
		if (call.getArguments().isEmpty())
			return null;
		for (int i = 0; i < call.getArguments().size(); ++i) {
			final GadgetType trail = GadgetTypes.inst().safeValueOf(call.getArguments().get(i).toLowerCase());
			if (trail != null) {
				call.getArguments().remove(i);
				return trail;
			}
		}
		return null;
	}

	@Override
	public List<String> tabComplete(CommandCall call) {
		return GadgetTypes.inst().values().stream().map(GadgetType::getId).sorted(String::compareTo).collect(Collectors.toList());
	}

}
