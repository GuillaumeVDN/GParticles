package com.guillaumevdn.gparticles.lib.particle.displayer.active;

import com.guillaumevdn.gcore.lib.function.ThrowableRunnable;

/**
 * @author GuillaumeVDN
 */
public abstract interface ParticleRunner extends ThrowableRunnable {
}
