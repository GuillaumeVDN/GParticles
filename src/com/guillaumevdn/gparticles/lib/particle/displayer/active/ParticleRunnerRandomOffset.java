package com.guillaumevdn.gparticles.lib.particle.displayer.active;

import java.util.List;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcore.lib.collection.CollectionUtils;
import com.guillaumevdn.gcore.lib.compatibility.particle.Particle;
import com.guillaumevdn.gcore.lib.number.NumberUtils;
import com.guillaumevdn.gcore.lib.string.placeholder.Replacer;
import com.guillaumevdn.gparticles.lib.particle.displayer.element.particle.ElementDisplayerParticle;
import com.guillaumevdn.gparticles.lib.particle.displayer.element.particle.ElementDisplayerParticleList;

/**
 * @author GuillaumeVDN
 */
public class ParticleRunnerRandomOffset implements ParticleRunner {

	private final Player player;
	private final Replacer rep;
	private final double offsetXMin;
	private final double offsetXMax;
	private final double offsetZMin;
	private final double offsetZMax;
	private final double offsetYMin;
	private final double offsetYMax;
	private final int delayTicks;
	private final int amount;
	private final ElementDisplayerParticleList particles;

	private int remainingTicks = 0;

	public ParticleRunnerRandomOffset(Player player, double offsetXMin, double offsetXMax, double offsetZMin, double offsetZMax, double offsetYMin, double offsetYMax, int delayTicks, int amount, ElementDisplayerParticleList particles) {
		this.player = player;
		this.rep = Replacer.of(player);
		this.offsetXMin = offsetXMin;
		this.offsetXMax = offsetXMax;
		this.offsetZMin = offsetZMin;
		this.offsetZMax = offsetZMax;
		this.offsetYMin = offsetYMin;
		this.offsetYMax = offsetYMax;
		this.delayTicks = delayTicks;
		this.amount = amount;
		this.particles = particles;
	}

	@Override
	public void run() throws Throwable {

		if (--remainingTicks <= 0) {
			remainingTicks = delayTicks;

			final List<Player> players = player.getWorld().getPlayers();
			for (int i = 0; i < amount; ++i) {
				final ElementDisplayerParticle part = CollectionUtils.random(particles.values());
				final Particle particle = part.getParticle().parse(rep).orNull();

				if (particle != null) {

					final Location loc = player.getLocation().add(
							NumberUtils.random(offsetXMin, offsetXMax),
							NumberUtils.random(offsetYMin, offsetYMax),
							NumberUtils.random(offsetZMin, offsetZMax)
							);

					Color color = null;
					Float redstoneColorScale = null;
					Integer noteColor = null;

					if (particle.getId().equals("REDSTONE")) {
						redstoneColorScale = part.getRedstoneColorScale().parse(rep).orNull();
					} else if (particle.getData().isMusicNote()) {
						noteColor = part.getParticleNoteColor().parse(rep).orNull();
					} else if (particle.getData().isColorable()) {
						final Integer red = part.getColorRed().parse(rep).orNull();
						final Integer green = part.getColorGreen().parse(rep).orNull();
						final Integer blue = part.getColorBlue().parse(rep).orNull();

						if (red != null && green != null && blue != null) {
							color = Color.fromRGB(red, green, blue);
						}
					}

					particle.send(players, loc, color, redstoneColorScale, noteColor, 1, 0f);
				}
			}
		}
	}

}
