package com.guillaumevdn.gparticles.lib.particle.displayer.element.particle;

import com.guillaumevdn.gcore.lib.compatibility.material.CommonMats;
import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.element.struct.Element;
import com.guillaumevdn.gcore.lib.element.struct.Need;
import com.guillaumevdn.gcore.lib.element.struct.list.ListElement;
import com.guillaumevdn.gcore.lib.string.Text;

/**
 * @author GuillaumeVDN
 */
public class ElementDisplayerParticleList extends ListElement<ElementDisplayerParticle> {

	public ElementDisplayerParticleList(Element parent, String id, Need need, Text editorDescription) {
		super(true, parent, id, need, editorDescription);
	}

	@Override
	protected ElementDisplayerParticle createElement(String elementId) {
		return new ElementDisplayerParticle(this, elementId, Need.optional(), null);
	}

	@Override
	public Mat editorIconType() {
		return CommonMats.EMERALD;
	}

}
