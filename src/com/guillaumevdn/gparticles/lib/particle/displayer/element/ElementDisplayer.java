package com.guillaumevdn.gparticles.lib.particle.displayer.element;

import com.guillaumevdn.gcore.lib.element.struct.Element;
import com.guillaumevdn.gcore.lib.element.struct.Need;
import com.guillaumevdn.gcore.lib.element.struct.container.typable.ElementTypableElementType;
import com.guillaumevdn.gcore.lib.element.struct.container.typable.TypableContainerElement;
import com.guillaumevdn.gcore.lib.string.Text;
import com.guillaumevdn.gparticles.lib.particle.displayer.types.DisplayerTypes;

/**
 * @author GuillaumeVDN
 */
public class ElementDisplayer extends TypableContainerElement<DisplayerType> {

	public ElementDisplayer(Element parent, String id, Need need, Text editorDescription) {
		super(DisplayerTypes.inst(), parent, id, need, editorDescription);
	}

	@Override
	protected final ElementTypableElementType<DisplayerType> addType() {
		return add(new ElementDisplayerType(this, "type", null));
	}

}
