package com.guillaumevdn.gparticles.lib.particle.displayer.types;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcore.lib.compatibility.material.CommonMats;
import com.guillaumevdn.gcore.lib.element.struct.Need;
import com.guillaumevdn.gcore.lib.string.placeholder.Replacer;
import com.guillaumevdn.gcore.lib.time.TimeUnit;
import com.guillaumevdn.gparticles.TextEditorGP;
import com.guillaumevdn.gparticles.lib.particle.displayer.active.ParticleRunner;
import com.guillaumevdn.gparticles.lib.particle.displayer.active.ParticleRunnerRandomOffset;
import com.guillaumevdn.gparticles.lib.particle.displayer.element.DisplayerType;
import com.guillaumevdn.gparticles.lib.particle.displayer.element.ElementDisplayer;
import com.guillaumevdn.gparticles.lib.particle.displayer.element.particle.ElementDisplayerParticleList;

/**
 * @author GuillaumeVDN
 */
public final class TypeRandomOffset extends DisplayerType {

	public TypeRandomOffset(String id) {
		super(id, CommonMats.BEDROCK);
	}

	@Override
	protected void doFillTypeSpecificElements(ElementDisplayer elem) {
		elem.addDouble("offset_x_min", Need.optional(-1d), TextEditorGP.elementParticleDisplayerOffsetOffsetXMin);
		elem.addDouble("offset_x_max", Need.optional(1d), TextEditorGP.elementParticleDisplayerOffsetOffsetXMax);
		elem.addDouble("offset_z_min", Need.optional(-1d), TextEditorGP.elementParticleDisplayerOffsetOffsetZMin);
		elem.addDouble("offset_z_max", Need.optional(1d), TextEditorGP.elementParticleDisplayerOffsetOffsetZMax);
		elem.addDouble("offset_y_min", Need.optional(-0.5d), TextEditorGP.elementParticleDisplayerOffsetOffsetYMin);
		elem.addDouble("offset_y_max", Need.optional(2.5d), TextEditorGP.elementParticleDisplayerOffsetOffsetYMax);
		elem.addDuration("delay", Need.optional(), 5, TimeUnit.TICK, TextEditorGP.elementParticleDisplayerOffsetDelay);
		elem.addInteger("amount", Need.optional(2), TextEditorGP.elementParticleDisplayerOffsetAmount);
		elem.add(new ElementDisplayerParticleList(elem, "particles", Need.required(), TextEditorGP.elementParticleDisplayerOffsetParticles));
	}

	@Override
	public ParticleRunner buildRunner(Player player, ElementDisplayer elem) {
		final Replacer rep = Replacer.of(player);
		final double offsetXMin = elem.directParseOrNull("offset_x_min", rep);
		final double offsetXMax = elem.directParseOrNull("offset_x_max", rep);
		final double offsetZMin = elem.directParseOrNull("offset_z_min", rep);
		final double offsetZMax = elem.directParseOrNull("offset_z_max", rep);
		final double offsetYMin = elem.directParseOrNull("offset_y_min", rep);
		final double offsetYMax = elem.directParseOrNull("offset_y_max", rep);
		final int delayTicks = (int) (elem.directParseOrElse("delay", rep, 250L) / 50L);
		final int amount = elem.directParseOrNull("amount", rep);
		final ElementDisplayerParticleList particles = elem.getElementAs("particles");

		return new ParticleRunnerRandomOffset(player, offsetXMin, offsetXMax, offsetZMin, offsetZMax, offsetYMin, offsetYMax, delayTicks, amount, particles);
	}

}
