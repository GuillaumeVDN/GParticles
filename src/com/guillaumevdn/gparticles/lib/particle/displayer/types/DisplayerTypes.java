package com.guillaumevdn.gparticles.lib.particle.displayer.types;

import com.guillaumevdn.gcore.lib.element.struct.container.typable.TypableElementTypes;
import com.guillaumevdn.gparticles.GParticles;
import com.guillaumevdn.gparticles.lib.particle.displayer.element.DisplayerType;

/**
 * @author GuillaumeVDN
 */
public final class DisplayerTypes extends TypableElementTypes<DisplayerType> {

	public DisplayerTypes() {
		super(DisplayerType.class);
	}

	// ----- types
	public final TypeRandomOffset 		RANDOM_OFFSET		= register(new TypeRandomOffset("RANDOM_OFFSET"));

	// ----- values
	public static DisplayerTypes inst() {
		return GParticles.inst().getDisplayerTypes();
	}

	@Override
	public DisplayerType defaultValue() {
		return RANDOM_OFFSET;
	}

}
