package com.guillaumevdn.gparticles.lib.particle;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.guillaumevdn.gcore.GCore;
import com.guillaumevdn.gcore.lib.GPlugin;
import com.guillaumevdn.gcore.lib.configuration.YMLConfiguration;
import com.guillaumevdn.gcore.lib.element.struct.Need;
import com.guillaumevdn.gcore.lib.element.struct.SuperElement;
import com.guillaumevdn.gcore.lib.element.struct.container.ContainerElement;
import com.guillaumevdn.gcore.lib.element.type.basic.ElementPermission;
import com.guillaumevdn.gcore.lib.element.type.basic.ElementString;
import com.guillaumevdn.gparticles.TextEditorGP;
import com.guillaumevdn.gparticles.lib.particle.displayer.element.ElementDisplayer;

/**
 * @author GuillaumeVDN
 */
public class ElementParticleGP extends ContainerElement implements SuperElement {

	private ElementString name = addString("name", Need.required(), TextEditorGP.elementParticleName);
	private ElementPermission permission = addPermission("permission", Need.optional(), TextEditorGP.elementParticlePermission);
	private ElementDisplayer displayer = add(new ElementDisplayer(this, "displayer", Need.required(), TextEditorGP.elementParticleDisplayer));

	public ElementParticleGP(File file, String id) {
		super(null, id, Need.optional(), null);
		this.file = file;
	}

	public ElementString getName() {
		return name;
	}

	public ElementPermission getPermission() {
		return permission;
	}

	public ElementDisplayer getDisplayer() {
		return displayer;
	}

	// ----- super element

	private final File file;
	private final List<String> loadErrors = new ArrayList<>();
	private transient YMLConfiguration config = null;

	@Override public GPlugin getPlugin() { return GCore.inst(); }
	@Override public File getOwnFile() { return null; }
	@Override public List<String> getLoadErrors() { return Collections.unmodifiableList(loadErrors); }
	@Override public YMLConfiguration getConfiguration() { if (config == null) { reloadConfiguration(); } return config; }
	@Override public String getConfigurationPath() { return "particles." + getId(); }
	@Override public void addLoadError(String error) { loadErrors.add(error); }
	@Override public void reloadConfiguration() { this.config = new YMLConfiguration(getPlugin(), file); }

}
