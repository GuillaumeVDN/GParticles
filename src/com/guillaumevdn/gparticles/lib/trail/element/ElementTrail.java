package com.guillaumevdn.gparticles.lib.trail.element;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.guillaumevdn.gcore.GCore;
import com.guillaumevdn.gcore.lib.GPlugin;
import com.guillaumevdn.gcore.lib.configuration.YMLConfiguration;
import com.guillaumevdn.gcore.lib.element.struct.Need;
import com.guillaumevdn.gcore.lib.element.struct.SuperElement;
import com.guillaumevdn.gcore.lib.element.struct.container.ContainerElement;
import com.guillaumevdn.gcore.lib.element.type.basic.ElementInteger;
import com.guillaumevdn.gcore.lib.element.type.basic.ElementMatList;
import com.guillaumevdn.gcore.lib.element.type.basic.ElementPermission;
import com.guillaumevdn.gcore.lib.element.type.basic.ElementString;
import com.guillaumevdn.gcore.lib.time.TimeUnit;
import com.guillaumevdn.gcore.lib.time.duration.ElementDuration;
import com.guillaumevdn.gparticles.TextEditorGP;

/**
 * @author GuillaumeVDN
 */
public class ElementTrail extends ContainerElement implements SuperElement {

	private ElementString name = addString("name", Need.required(), TextEditorGP.elementTrailName);
	private ElementPermission permission = addPermission("permission", Need.optional(), TextEditorGP.elementTrailPermission);
	private ElementDuration blockChangeDelay = addDuration("block_change_delay", Need.optional(), 4, TimeUnit.TICK, TextEditorGP.elementTrailBlockChangeDelay);
	private ElementDuration blockPersistenceTime = addDuration("block_persistence_time", Need.optional(), 3, TimeUnit.SECOND, TextEditorGP.elementTrailBlockPersistenceTime);
	private ElementMatList blockTypes = addMatList("block_types", Need.required(), TextEditorGP.elementTrailBlockTypes);
	private ElementInteger verticalOffset = addInteger("vertical_offset", Need.optional(0), TextEditorGP.elementTrailVerticalOffset);

	public ElementTrail(File file, String id) {
		super(null, id, Need.optional(), null);
		this.file = file;
	}

	public ElementString getName() {
		return name;
	}

	public ElementPermission getPermission() {
		return permission;
	}

	public ElementDuration getBlockChangeDelay() {
		return blockChangeDelay;
	}

	public ElementDuration getBlockPersistenceTime() {
		return blockPersistenceTime;
	}

	public ElementMatList getBlockTypes() {
		return blockTypes;
	}

	public ElementInteger getVerticalOffset() {
		return verticalOffset;
	}

	// ----- super element

	private final File file;
	private final List<String> loadErrors = new ArrayList<>();
	private transient YMLConfiguration config = null;

	@Override public GPlugin getPlugin() { return GCore.inst(); }
	@Override public File getOwnFile() { return null; }
	@Override public List<String> getLoadErrors() { return Collections.unmodifiableList(loadErrors); }
	@Override public YMLConfiguration getConfiguration() { if (config == null) { reloadConfiguration(); } return config; }
	@Override public String getConfigurationPath() { return "trails." + getId(); }
	@Override public void addLoadError(String error) { loadErrors.add(error); }
	@Override public void reloadConfiguration() { this.config = new YMLConfiguration(getPlugin(), file); }

}
