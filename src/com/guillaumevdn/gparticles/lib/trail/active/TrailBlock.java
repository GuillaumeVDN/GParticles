package com.guillaumevdn.gparticles.lib.trail.active;

import org.bukkit.block.Block;

import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.location.Point;

public class TrailBlock {

	private final Point point;
	private long changedAt;

	public TrailBlock(Point point) {
		this.point = point;
		this.changedAt = System.currentTimeMillis();
	}

	public Point getPoint() {
		return point;
	}

	public long getChangedAt() {
		return changedAt;
	}

	public void setChangedAt(long changedAt) {
		this.changedAt = changedAt;
	}

	public void restore() {
		final Block block = point.toBlock();
		final Mat mat = Mat.fromBlock(block).orAir();
		mat.setBlockChange(block);
	}
}
