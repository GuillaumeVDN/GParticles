package com.guillaumevdn.gparticles.lib.trail.active;

import java.util.List;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcore.lib.collection.CollectionUtils;
import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.concurrency.RWHashMap;
import com.guillaumevdn.gcore.lib.function.ThrowableRunnable;
import com.guillaumevdn.gcore.lib.location.Point;
import com.guillaumevdn.gcore.lib.string.placeholder.Replacer;
import com.guillaumevdn.gparticles.ConfigGP;
import com.guillaumevdn.gparticles.GParticles;
import com.guillaumevdn.gparticles.lib.trail.element.ElementTrail;

/**
 * @author GuillaumeVDN
 */
public class TrailRunner implements ThrowableRunnable {

	private final Player player;
	private final List<Mat> blockTypes;
	private final int verticalOffset;
	private final long blockPersistenceMillis;
	private final int blockChangeDelayTicks;

	private RWHashMap<Point, TrailBlock> trailBlocks = new RWHashMap<>(5, 1f);
	private int ticksRemaining = 0;

	public TrailRunner(Player player, ElementTrail trail) {
		this.player = player;

		final Replacer rep = Replacer.of(player);
		this.blockTypes = trail.getBlockTypes().parse(rep).orEmptyList();
		this.verticalOffset = trail.getVerticalOffset().parse(rep).orElse(0);
		this.blockChangeDelayTicks = (int) (trail.getBlockChangeDelay().parse(rep).orElse(200L) / 50L);
		this.blockPersistenceMillis = trail.getBlockPersistenceTime().parse(rep).orElse(3000L);
	}

	public TrailRunner(Player player, List<Mat> blockTypes, int verticalOffset, int blockChangeDelayTicks, long blockPersistenceMillis) {
		this.player = player;
		this.blockTypes = blockTypes;
		this.verticalOffset = verticalOffset;
		this.blockChangeDelayTicks = blockChangeDelayTicks;
		this.blockPersistenceMillis = blockPersistenceMillis;
	}

	@Override
	public void run() throws Throwable {

		if (--ticksRemaining > 0) {
			return;
		}
		if (blockTypes.isEmpty()) {
			return;
		}

		// make sure player is standing on a block (avoid bridging using the trail)
		if (!player.isOnGround()) {
			return;
		}

		final Block blockBelow = player.getLocation().getBlock().getRelative(0, -1, 0);
		final Mat blockBelowMat = Mat.fromBlock(blockBelow).orAir();

		if (blockBelowMat.getData().isTraversable()) {
			return;
		}

		// make sure the trail block itself is allowed
		final Block target = blockBelow.getRelative(0, verticalOffset, 0);
		final Mat targetMat = Mat.fromBlock(target).orAir();

		if (verticalOffset != 0 ? !targetMat.isAir() : ConfigGP.trailBlocksBlacklist.stream().anyMatch(ignore -> targetMat.getData().getDataName().contains(ignore))) {
			return;
		}

		// set trail block
		final Point point = new Point(target);
		final TrailBlock tblock = GParticles.inst().getTrailBlocks().compute(point, (__, tb) -> {
			if (tb != null) {
				tb.setChangedAt(System.currentTimeMillis());
			} else {
				tb = new TrailBlock(point);
			}
			return tb;
		});
		trailBlocks.computeIfAbsent(point, __ -> tblock);

		final Mat trailMat = CollectionUtils.random(blockTypes);
		trailMat.setBlockChange(target);

		// remove old trail blocks
		trailBlocks.iterateAndModify((p, tb, it) -> {
			if (System.currentTimeMillis() - tb.getChangedAt() >= blockPersistenceMillis) {
				tb.restore();
				it.remove();
				GParticles.inst().getTrailBlocks().remove(p);
			}
		});

		// mark ticks remaining
		ticksRemaining = blockChangeDelayTicks;
	}

	public void cancel() {
		trailBlocks.forEach((point, tblock) -> {
			tblock.restore();
			GParticles.inst().getTrailBlocks().remove(point);
		});
	}

}
